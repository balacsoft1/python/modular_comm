# modular_com

## What is Modular Com ?

Module Com is a set of components which allow emulating a POS (Point Of Sale). It is as Micro services oriented solution. Note that components are developed in Python v3, while skin is developed in Java-Fx.

## Content

The list of components below are composing the modular com.

### Components

- m_tradi (8000)         : translation component (server component)
- m_ihm (8001)           : display component (server component)
- m_icc (8002)           : smart card component (server component)
- m_dep (8003)           : Dependencies component (server component)
- m_seq (8004)           : Sequencer component (server component) - MASTER MODULE
- m_print (8005)         : Printing component (server component)
- m_mag (8006)           : Magnetic Stripe component (server component)
- m_ecr (8007)           : Electronic Cash Register component (server component)
- m_comm (8008)          : Communication component - Transport Layer (server component)
- m_host (8009)          : Host communication component - Appli Layer (server component)
- m_screen (8010)        : Screen Display Component (server component)

### Applications

- appli_test             : command line test application (client component)
- appli_win              : graphical test application (client component) - obsolete

## Architecture

Each components can communicate with another thanks to TCP/IP protocol and JSON format.
The JSON command contains a header : Command and Version, and a body with the data.

Each component has its own parameter file : param.json.

## Application usage

Start "pos_emu.bat" 
This will launch the skin java component in 'skin' repository and all the modules listed above
Do not close the console window as it contains all the modules servers
The master component is m_seq (Sequencer) which will be started at the end
