#!/usr/bin/env python3

import json
import socket
import sys
import os
import ast

# add common directory in the path, and import common class
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
main_path = os.path.dirname(os.path.dirname(CURRENT_DIR))
# main path is the directory of modular_com (where the README is present)
sys.path.append(main_path)
from modules.lib_common.common import c_common

######################### CONSTANTS ##################################

cmd_disp_logo = {"cmd" : "LOGO","version" : "1.0.0"}

########################### GET_COMMAND ##############################
def get_command(message_received):
    the_msg_command = message_received["command"]
    return the_msg_command

########################### GET_VERSION ##############################
def get_version(str_module_version):
    ret_code = c_common.RET_OK
    answer_msg = str_module_version
    json_answer_msg = {'rsp': c_common.CMD_GET_VERSION,
                       'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg

########################### MODULE INIT ##############################
def module_initialization(json_data):
    ret_code = c_common.RET_OK
    answer_msg = my_common.get_str_error_code(ret_code)
    json_answer_msg = {'rsp':c_common.CMD_MODULE_INIT,
                       'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg

########################### DISPLAY LOGO ##############################
def display_logo(json_data):
    ret_code = c_common.RET_OK
    str_received = ""

    # call other modules to get their version
    target_module = 'pos_emu'
    error_code, target_ip, target_port = my_common.get_ip_port(target_module, this_module_json_data) 

    if error_code == 0:
        # get version
        json_cmd = json.dumps(cmd_disp_logo)
        error_code, json_message_received = my_common.send_tcpip_message(target_module, target_ip, target_port, json_cmd)
        if error_code == 0:
            # extract the response
            str_received = json_message_received['msg']
        else:
            ret_code = c_common.RET_ERR_SOCKET_CONNEXION
            str_received = my_common.get_str_error_code(ret_code)

    # call the POS emulator to display the logo
    json_answer_msg = {'rsp':c_common.CMD_DISP_LOGO,
                       'retcode': ret_code, 'msg': str_received}
    return ret_code, json_answer_msg

###################################################################################################
# Translate : translate a given message

def display_msg(msg):
    try:
        if len(msg)<32:
            print(msg)
            func_ret = 0
        else:
            func_ret = -1

    finally:
        return func_ret

###################################################################################################
###################################################################################################
###################################################################################################
# Module start
#

#name of this module
module_name = "m_ihm"
my_common = c_common(module_name)

# start the module
error_code, this_module_json_data, port = my_common.start_module(module_name)
if (error_code != 0):
    exit()

# get module version
this_module_version = this_module_json_data["version"]

# set the level of log
log_level = this_module_json_data["log"]
my_common.set_log_level(log_level)
my_common.print_log ('Module Start (log level=' + str(log_level) + ')')

###################################################################################################
# TCP/IP listener

try:
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('localhost', port)
    my_common.print_log ('starting up on ' + server_address[0] + ' port ' +  str(server_address[1]))
    sock.bind(server_address)

except:
    my_common.print_log('Error connection socket')
    sys.exit()
    
# Listen for incoming connections
try:
    sock.listen(1)
    sock.settimeout(5)
    while True:
        my_common.print_log ('waiting for a connection')
        try:
            connection, client_address = sock.accept()
            # 1024 = max size of received data
            response = connection.recv(c_common.TCP_RECEPTION_BUFFER_SIZE)
            if response != "":
                # parse received message and get the command
                data_received = response.decode("utf-8", "ignore")
                json_dict = ast.literal_eval(data_received)
                command_to_execute = get_command(json_dict)
                my_common.print_log ("Received =" + data_received)

                # manage the command
                if (command_to_execute == c_common.CMD_GET_VERSION):
                    ret_code, json_my_answer = get_version(
                        this_module_version)
                elif (command_to_execute == c_common.CMD_MODULE_INIT):
                    ret_code, json_my_answer = module_initialization(json_dict)
                elif (command_to_execute == c_common.CMD_DISP_LOGO):
                    ret_code, json_my_answer = display_logo(json_dict)
                else:
                    ret_code = c_common.RET_ERR_UNKNOWN_COMMAND
                    answer_msg = my_common.get_str_error_code(c_common.RET_ERR_UNKNOWN_COMMAND)
                    json_my_answer = {'rsp':command_to_execute,'retcode': ret_code, 'msg': answer_msg}

                # answer to caller
                str_msg_to_send = str(json_my_answer)
                msg_to_translate_bytes = str_msg_to_send + '\n'
                message_to_translate_bytes = msg_to_translate_bytes.encode()
                my_common.print_log ("Sent =" + msg_to_translate_bytes)
                connection.sendall(message_to_translate_bytes)

        except socket.timeout:
            # do nothing, start reception again (this allows to catch other exceptions like CTRL+C)
            pass

except KeyboardInterrupt:
    my_common.print_log_error('Program interrupted by user')
except:
    my_common.print_log_error('error unknown')
finally:
    my_common.print_log ("Close")
    connection.close()
    sys.exit()