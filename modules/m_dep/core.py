#!/usr/bin/env python3

import json
import socket
import sys
import os
import ast

# add common directory in the path, and import common class
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
main_path = os.path.dirname(os.path.dirname(CURRENT_DIR))
# main path is the directory of modular_com (where the README is present)
sys.path.append(main_path)
from modules.lib_common.common import c_common
from modules.lib_common.definitions import c_definitions

###################################################################################################
# CONSTANTS

cmd_get_version = {"id" : "m_dep","version": "0.1.0","command" : "CMD_GET_VERS"}
                    
###################################################################################################
# Manage Dependencies

########################### GET_COMMAND ##############################
def get_command(message_received):
    the_msg_command = message_received["command"]
    return the_msg_command


########################### GET_VERSION ##############################
def get_version(str_module_version):
    ret_code = c_common.RET_OK
    answer_msg = str_module_version
    json_answer_msg = {'rsp': c_common.CMD_GET_VERSION,
                       'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg


########################### CHECK_DEPENDENCY #########################
def check_dependency(message_received):
    ret_code = c_common.RET_OK

    # check the minimum versions
    i = 0
    received_module = ""
    while(received_module != "end"):
        received_module = message_received['modules'][i][0]['module']
        if (received_module == "end"):
            break

        # initialize versions
        received_module_version = c_definitions(int(message_received['modules'][i][1]['major']), int(message_received['modules'][i][2]['minor']), 0)

        # first check modules
        ret_code = check_module_dependency(received_module, received_module_version)
        if ret_code != 0:
            break

        # To next module
        i += 1

    answer_msg = my_common.get_str_error_code(ret_code)
    json_answer_msg = {'rsp': c_common.CMD_CHECK_DEPENDENCY,'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg


############
def compare_versions(version, version_dep):
    if (version.version_major > version_dep.version_major):
        ret_code = c_common.RET_OK
    elif (version.version_major < version_dep.version_major):
        ret_code = c_common.RET_ERR_DEPENDENCY_ERROR
    else:
        # major versions are identical, check minor versions
        if (version.version_minor > version_dep.version_minor):
            ret_code = c_common.RET_OK
        elif (version.version_minor < version_dep.version_minor):
            ret_code = c_common.RET_ERR_DEPENDENCY_ERROR
        else:
            # major and minor versions are identical, so we consider it is OK
            ret_code = c_common.RET_OK

    return ret_code


############
def check_module_dependency(module_to_check, mod_version):
    index = 0
    for i in range(nb_module):
        if (module_to_check == module_ver_name[i]):
            index = i
            break

    return compare_versions(module_ver[index], mod_version)


########################### MODULE INIT ##############################

############
def module_initialization(json_data):
    ret_code = c_common.RET_OK

    # Get version of all internal modules
    i = 0
    nbModule = 0
    module_to_get_version = ""
    while(module_to_get_version != "end"):
        module_to_get_version = this_module_json_data['modules'][i][0]['module']
        if (module_to_get_version == 'end'):
            break
        
        if (module_to_get_version != module_name):
            ver = call_other_module(module_to_get_version, json_data)
            module_ver[i] = ver
            module_ver_name[i] = module_to_get_version
            nbModule += 1
            
        i += 1
    
    # get version of all system components
    i = 0
    module_to_get_version = ""
    while(module_to_get_version != "end"):
        module_to_get_version = this_module_json_data['systems'][i]['system']
        if (module_to_get_version == 'end'):
            break
        
        ver = call_other_system(module_to_get_version, json_data)
        module_ver[i + nbModule] = ver
        module_ver_name[i + nbModule] = module_to_get_version
        i += 1
    
    nbSystem = i

    # Answer
    answer_msg = 'NB modules=' + str(nbModule) + ' - Nb System=' + str(nbSystem)
    json_answer_msg = {'rsp':c_common.CMD_MODULE_INIT,'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg

############
def call_other_system(target_module, json_data):
    other_module_version = c_definitions(0, 0, 0)

    if target_module == 'os':
        other_module_version = os_version
    elif target_module == 'manager':
        other_module_version = manager_version
    elif target_module == 'ep2emv':
        other_module_version = ep2emv_version
    elif target_module == 'ep2cless':
        other_module_version = ep2cless_version

    return other_module_version

############
def call_other_module(target_module, json_data):
    other_module_version = c_definitions(0, 0, 0)
    # call other modules to get their version
    error_code, target_ip, target_port = my_common.get_ip_port(target_module, this_module_json_data) 

    if error_code == 0:
        # get version
        json_cmd = json.dumps(cmd_get_version)
        error_code, json_message_received = my_common.send_tcpip_message(target_module, target_ip, target_port, json_cmd)
        if error_code == 0:
            # extract the version
            str_version = json_message_received['msg']
            # module version 
            other_module_version.get_version_from_string(str_version)
            my_common.print_log_info("GET VERSION - module '" + target_module + "'-version=" + str_version)
        else:
            my_common.print_log_error("GET VERSION - module '" + target_module + "' not reachable")
            
    return other_module_version


###################################################################################################
###################################################################################################
###################################################################################################
# Module start
#

# name of this module
module_name = "m_dep"
my_common = c_common(module_name)

# start the module
error_code, this_module_json_data, port = my_common.start_module(module_name)
if (error_code != 0):
    exit()

# display language
my_common.print_log('language=' + this_module_json_data["lang"])
this_module_version = this_module_json_data["version"]

# set the level of log
log_level = this_module_json_data["log"]
my_common.set_log_level(log_level)
my_common.print_log ('Module Start (log level=' + str(log_level) + ')')

# modules version will be stored in this array after the round of table
nb_module = 40
module_ver = []
module_ver_name = []
for i in range(nb_module):
    module_ver.append(c_definitions(0,0,0))
    module_ver_name.append("")

# first get low-level layer versions
# this part has to be changed with real OS calls to get the component versions
os_version = c_definitions(9, 0, 0)
manager_version = c_definitions(80, 0, 0)
ep2emv_version = c_definitions(22, 8, 1)
ep2cless_version = c_definitions(7, 0, 0)

###################################################################################################
# TCP/IP listener

# wait for command
try:
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('localhost', port)
    my_common.print_log('starting up on ' +
                        server_address[0] + ' port ' + str(server_address[1]))
    sock.bind(server_address)

except:
    my_common.print_log('Error connection socket')
    exit()

# Listen for incoming connections
try:

    sock.listen(1)
    sock.settimeout(5)
    while True:
        my_common.print_log('waiting for a connection')
        try:
            connection, client_address = sock.accept()
            # 1024 = max size of received data
            response = connection.recv(c_common.TCP_RECEPTION_BUFFER_SIZE)
            if response != "":
                # parse received message and get the command
                data_received = response.decode("utf-8", "ignore")
                json_dict = ast.literal_eval(data_received)
                command_to_execute = get_command(json_dict)
                my_common.print_log ("Received =" + data_received)

                # manage the command
                if (command_to_execute == c_common.CMD_GET_VERSION):
                    ret_code, json_my_answer = get_version(
                        this_module_version)
                elif (command_to_execute == c_common.CMD_CHECK_DEPENDENCY):
                    ret_code, json_my_answer = check_dependency(
                        json_dict)
                elif (command_to_execute == c_common.CMD_MODULE_INIT):
                    ret_code, json_my_answer = module_initialization(json_dict)
                else:
                    ret_code = c_common.RET_ERR_UNKNOWN_COMMAND
                    answer_msg = my_common.get_str_error_code(c_common.RET_ERR_UNKNOWN_COMMAND)
                    json_my_answer = {'rsp':command_to_execute,'retcode': ret_code, 'msg': answer_msg}

                # answer to caller
                str_msg_to_send = str(json_my_answer)
                msg_to_translate_bytes = str_msg_to_send + '\n'
                message_to_translate_bytes = msg_to_translate_bytes.encode()
                my_common.print_log ("Sent =" + msg_to_translate_bytes)
                connection.sendall(message_to_translate_bytes)
                
        except socket.timeout:
            # do nothing, start reception again (this allows to catch other exceptions like CTRL+C)
            pass

except KeyboardInterrupt:
    my_common.print_log_error('Program interrupted by user')
except:
    my_common.print_log_error('error unknown')
finally:
    my_common.print_log("Close")
    connection.close()
    sys.exit()
