#!/usr/bin/env python3

import sys
import socket
import datetime
import os
import json
import pickle
import ast

###################################################################################################
# Class c_common
class c_common:

    ###################################################################################################
    # Constants
    ###################################################################################################

    # TCP/IP parameters
    TCP_RECEPTION_BUFFER_SIZE           = 4096

    # Error codes
    RET_OK                              = 0
    RET_ERR_MSG_TOO_LONG                = -1
    RET_ERR_JSON_MODULE_NAME_INCORRECT  = -2
    RET_ERR_MODULE_INVALID              = -3
    RET_ERR_SOCKET_CONNEXION            = -4
    RET_ERR_UNKNOWN_COMMAND             = -5
    RET_ERR_INVALID_COMMAND             = -6
    RET_ERR_DEPENDENCY_ERROR            = -7

    # Commands
    CMD_CHECK_DEPENDENCY                = "CMD_CHECK_DEP"
    CMD_GET_VERSION                     = "CMD_GET_VERS"
    CMD_MODULE_INIT                     = "CMD_INIT"
    CMD_ICC_ENABLE                      = "CMD_ICC_ENABLE"
    CMD_DISP_LOGO                       = "CMD_DISP_LOGO"

    # Log type
    LOG_TYPE_ERR                        = 1
    LOG_TYPE_WARN                       = 2
    LOG_TYPE_INFO                       = 3

    # Level of log
    LOG_LEVEL_NONE                      = 0
    LOG_LEVEL_ERR                       = 1
    LOG_LEVEL_WARNING                   = 2
    LOG_LEVEL_INFO                      = 3
    LOG_LEVEL_ALL                       = 4
    
    ###################################################################################################

    def __init__(self, name):
        self.module_name = name
        self.log_level = c_common.LOG_LEVEL_NONE
        self.STR_LOG_TYPE = ["", "*ERR", "WARN", "INFO"]

    ###################################################################################################

    # set the level of log in a file
    def set_log_level(self, the_log_level):
        self.log_level = the_log_level

    def write_log_in_file(self, log_to_add):
        filename = "..//..//logs/" + self.module_name + ".log"
        with open(filename, "a") as my_log_file:
            my_log_file.write(log_to_add + '\n')

    # prints a message to display

    def print_log(self, msg, log_type=3):
        the_date_time = str(datetime.datetime.now())
        log = the_date_time + " : " + self.module_name + " : " + self.STR_LOG_TYPE[log_type] + ": " + msg
        print(log)
        if (self.log_level >= log_type):
            self.write_log_in_file(log)

    def print_log_info(self, msg):
        self.print_log(msg, c_common.LOG_TYPE_INFO)

    def print_log_warning(self, msg):
        self.print_log(msg, c_common.LOG_TYPE_WARN)

    def print_log_error(self, msg):
        self.print_log(msg, c_common.LOG_TYPE_ERR)

    # returns a message to display

    def display_log(self, msg, type = 'INFO'):
        the_date_time = str(datetime.datetime.now())
        return (the_date_time + " : " + self.module_name + " : " + type + ": " + msg)

    def display_log_info(self, msg):
        return self.display_log(msg, "INFO")

    def display_log_warning(self, msg):
        return self.display_log(msg, "WARN")

    def display_log_error(self, msg):
        return self.display_log(msg, "*ERR")

###################################################################################################

    def get_str_error_code(self, error_val):
        try:
            if (error_val == c_common.RET_OK):
                func_str_ret = "OK"
            elif (error_val == c_common.RET_ERR_MSG_TOO_LONG):
                func_str_ret = "ERROR : Message too long"
            elif (error_val == c_common.RET_ERR_JSON_MODULE_NAME_INCORRECT):
                func_str_ret = "ERROR : json module name is different"
            elif (error_val == c_common.RET_ERR_MODULE_INVALID):
                func_str_ret = "ERROR : module does not exist"
            elif (error_val == c_common.RET_ERR_SOCKET_CONNEXION):
                func_str_ret = "ERROR : Error connexion socket"
            elif (error_val == c_common.RET_ERR_UNKNOWN_COMMAND):
                func_str_ret = "ERROR : Unknow command"                
            elif (error_val == c_common.RET_ERR_INVALID_COMMAND):
                func_str_ret = "ERROR : Invalid command"                
            elif (error_val == c_common.RET_ERR_DEPENDENCY_ERROR):
                func_str_ret = "ERROR : A dependency is not satisfied"                
            else:   
                func_str_ret = "ERROR : unkown error"

        finally:
            return func_str_ret

###################################################################################################
# dedicated for an application
# get ip and port from json file 

    def get_ip_port(self, module_name, data):
        i = 0
        error_code = 0
        ip = 0        
        port = 0

        component_name = 'modules'
        component_sub_name = 'module'
            
        i = 0
        module = ""
        while(module != "end"):
            module = data[component_name][i][0][component_sub_name]
            if module == module_name:
                ip = data[component_name][i][1]['ip']
                port = data[component_name][i][2]['port']
                break
            elif module == "end":
                error_code = c_common.RET_ERR_MODULE_INVALID
            i += 1

        return (error_code, ip, port)

###################################################################################################

    def start_module(self, the_module_name):
        # identify current repository, and move if it does not exist
        cwd = os.getcwd()
        self.print_log("Current Dir=" + cwd)

        # move to correct dir if required
        if (the_module_name[0] == 'm'):
            local_repo = 'modules'
        else:
            local_repo = 'appli'

        repo = "./" + local_repo + "/" + the_module_name
        if (os.path.isdir(repo) == True):
            os.chdir(repo)
            cwd = os.getcwd()
            self.print_log("Change Dir to=" + cwd)

        # open parameter file
        data = json.load(open('param.json'))

        #name of this module
        json_module_name = data["id"]
        if (json_module_name != the_module_name):
            error_code = -2
            self.print_log_error(self.get_str_error_code(error_code))         
        else:
            # print parameters
            error_code = 0
            module_type = data["type"]
            module_port = data["port"]
            self.print_log('----- module start -----')
            self.print_log('version=' + data["version"])
            self.print_log('type=' + module_type)
            self.print_log('port=' + str(module_port))

        return (error_code, data, module_port)

###################################################################################################

    def send_tcpip_message(self, module, ip_value, port_value, msg_to_send):
        error_code = 0
        json_data_received = {}

        # Get port for needed modules
        if port_value == 0:
            # module does not exist
            self.print_log_error(module + 'module does not exist !')
            error_code = c_common.RET_ERR_MODULE_INVALID
        else:
            try:
                # Create a TCP/IP socket
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Connect the socket to the port where the server is listening
                server_address = (ip_value, port_value)
                self.print_log('Send ' + module + ' (' + server_address[0] + ':' +  str(server_address[1]) + ')')
                sock.connect(server_address)

            except:
                self.print_log_error('Error connexion socket')
                error_code = -4

            if (error_code == 0):
                try:
                    # Send data
                    str_msg_to_send = str(msg_to_send)
                    msg_to_translate_bytes = str_msg_to_send + '\n'
                    message_to_translate_bytes = msg_to_translate_bytes.encode()
                    sock.sendall(message_to_translate_bytes)

                    # Look for the response
                    data = sock.recv(c_common.TCP_RECEPTION_BUFFER_SIZE)                    
                    data_received = data.decode("utf-8", "ignore")
                    json_data_received = ast.literal_eval(data_received)

                except:
                    self.print_log_error('Error decoding message to sent')
                    error_code = -6

                finally:
                    sock.close()

        return (error_code, json_data_received)

###################################################################################################
