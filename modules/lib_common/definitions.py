#!/usr/bin/env python3

import sys

###################################################################################################
# Constants
###################################################################################################


###################################################################################################

class c_definitions:
    version_major = 0
    version_minor = 0
    version_patch = 0

    # version is 3 integer
    def __init__(self, major, minor, patch):
        self.version_major = major
        self.version_minor = minor
        self.version_patch = patch
        
    # version is in a string : format X.Y.Z
    # xxx.find("Y", A) : allows to find position of the character 'Y' in string 'xxx' starting from position A
    # word[A:N] #get N characters of the word starting from offset A
    # word[2:5] #get 5 chars of the word starting from 2
    # word[4:] #get all remaining chars of the word starting from 4
    def get_version_from_string(self, str_version):
        index0 = str_version.find(".", 0)
        str_vers_maj = str_version[0:index0]
        index1 = str_version.find(".", index0+1)
        str_vers_min = str_version[index0+1:index1]
        str_vers_patch = str_version[index1+1:]
        self.version_major = int(str_vers_maj)
        self.version_minor = int(str_vers_min)
        self.version_patch = int(str_vers_patch)


###################################################################################################
