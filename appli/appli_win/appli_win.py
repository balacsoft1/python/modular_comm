#!/usr/bin/env python3

import sys
import os
from tkinter import Frame
import pygubu
import json

# add common directory in the path, and import common class
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
from lib_common.common import c_common

###################################################################################################
# CLASS

class appli_win(Frame):
    ###################################################################################################
    # Button Management

    def quit(self, event=None):
        self.mainwindow.quit()

    def run(self):
        self.mainwindow.mainloop()

    def output_msg(self, msg):
        # get variables
        self.obj_output.set(msg)
    
    def output_log_info(self, msg):
        out_msg = self.my_common.display_log_info(msg)
        self.output_msg(out_msg)

    def button_send_mngt(self):
        # first get the module to use (and its ip+port)
        target_module = self.obj_module_to_use.get()
        message_to_send = self.obj_message_to_send.get()
        error_code, target_ip, target_port = self.my_common.get_ip_port(target_module, self.json_data) 
        if error_code != 0:
            str_to_disp = self.my_common.get_str_error_code(error_code)
            self.output_msg(str_to_disp)
        else:            
            # get variables
            self.output_msg("target=" + target_module + " - ip=" + target_ip + ":" + str(target_port))
            error_code, message_received = self.my_common.send_tcpip_message(target_module, target_ip, target_port, message_to_send)
            self.output_log_info("error=" + str(error_code) + " - msg=" + message_received) 

    def button_clear_mngt(self):
        self.output_msg('')

    def button_quit_mngt(self):
        print("Quit")
        self.mainwindow.quit()

    ###################################################################################################
    # fenetre
    def __init__(self):

        #1: Create a builder
        self.builder = builder = pygubu.Builder()
        #2: Load an ui file
        builder.add_from_file(os.path.join(CURRENT_DIR, 'appli_ihm.ui'))      
        #3: Create the toplevel widget.
        self.mainwindow = builder.get_object('mainwindow')

        self.obj_module_to_use = builder.get_variable('str_module_to_use')
        self.obj_message_to_send = builder.get_variable('str_message_to_send')
        self.obj_output = builder.get_variable('str_output')
        
        # Configure callbacks
        callbacks = {
            'button_send_mngt': self.button_send_mngt,
            'button_clear_mngt': self.button_clear_mngt,
            'button_quit_mngt': self.button_quit_mngt
        }
        builder.connect_callbacks(callbacks)

        ###################################################################################################
        # Module start

        module_name = "appli_test"
        my_common = c_common(module_name)
        error_code, json_data, target_port = my_common.start_module(module_name)
        if (error_code != 0):
            exit()

        # put info in the main object
        self.my_common = my_common
        self.json_data = json_data

        # read data from json file
        self.output_log_info('language=' + json_data["lang"])
        self.output_log_info('target_port=' + str(target_port))

if __name__ == '__main__':
    app = appli_win()
    app.run()
