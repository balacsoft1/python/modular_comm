#!/usr/bin/env python3


import socket
import sys
import os
import ast

# add common directory in the path, and import common class
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
main_path = os.path.dirname(os.path.dirname(CURRENT_DIR))
# main path is the directory of modular_com (where the README is present)
sys.path.append(main_path)
from modules.lib_common.common import c_common

###################################################################################################
# private function

########################### GET_COMMAND ##############################
def get_command(message_received):
    the_msg_command = message_received["command"]
    return the_msg_command

########################### GET_VERSION ##############################
def get_version(str_module_version):
    ret_code = c_common.RET_OK
    answer_msg = str_module_version
    json_answer_msg = {'rsp': c_common.CMD_GET_VERSION,
                       'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg

########################### MODULE INIT ##############################
def module_initialization(json_data):
    ret_code = c_common.RET_OK
    answer_msg = my_common.get_str_error_code(ret_code)
    json_answer_msg = {'rsp':c_common.CMD_MODULE_INIT,
                       'retcode': ret_code, 'msg': answer_msg}
    return ret_code, json_answer_msg

###################################################################################################
# Module start

module_name = "appli_test"
my_common = c_common(module_name)
error_code, this_module_json_data, port = my_common.start_module(module_name)
if (error_code != 0):
    exit()

# read data from json file
my_common.print_log('language=' + this_module_json_data["lang"])
this_module_version = this_module_json_data["version"]

# set the level of log
log_level = this_module_json_data["log"]
my_common.set_log_level(log_level)
my_common.print_log ('Module Start (log level=' + str(log_level) + ')')

###################################################################################################
# TCP/IP listener

try:
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = ('localhost', port)
    my_common.print_log ('starting up on ' + server_address[0] + ' port ' +  str(server_address[1]))
    sock.bind(server_address)

except:
    my_common.print_log('Error connection socket')
    exit()

# Listen for incoming connections
try:
    
    sock.listen(1)
    sock.settimeout(5)
    while True:
        my_common.print_log ('waiting for a connection')
        try:
            connection, client_address = sock.accept()
            # 1024 = max size of received data
            response = connection.recv(c_common.TCP_RECEPTION_BUFFER_SIZE)
            if response != "":
                # parse received message and get the command
                data_received = response.decode("utf-8", "ignore")
                json_dict = ast.literal_eval(data_received)
                command_to_execute = get_command(json_dict)
                my_common.print_log ("Received =" + data_received)

                # manage the command
                if (command_to_execute == c_common.CMD_GET_VERSION):
                    ret_code, json_my_answer = get_version(this_module_version)
                elif (command_to_execute == c_common.CMD_MODULE_INIT):
                    ret_code, json_my_answer = module_initialization(json_dict)
                else:
                    ret_code = c_common.RET_ERR_UNKNOWN_COMMAND
                    answer_msg = my_common.get_str_error_code(c_common.RET_ERR_UNKNOWN_COMMAND)
                    json_my_answer = {'rsp':command_to_execute,'retcode': ret_code, 'msg': answer_msg}

                # answer to caller
                str_msg_to_send = str(json_my_answer)
                msg_to_translate_bytes = str_msg_to_send + '\n'
                message_to_translate_bytes = msg_to_translate_bytes.encode()
                my_common.print_log ("Sent =" + msg_to_translate_bytes)
                connection.sendall(message_to_translate_bytes)

        except socket.timeout:
            # do nothing, start reception again (this allows to catch other exceptions like CTRL+C)
            pass

except KeyboardInterrupt:
    my_common.print_log_error('Program interrupted by user')
except:
    my_common.print_log_error('error unknown')
finally:
    my_common.print_log ("Close")
    connection.close()
    sys.exit()
